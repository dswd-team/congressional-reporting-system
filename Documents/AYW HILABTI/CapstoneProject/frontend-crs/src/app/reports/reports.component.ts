import { Component, OnInit } from '@angular/core';
import { Program } from '../_models/program';
import { Province } from '../_models/province';
import { ProgramService } from '../_services/program.service';
import { ProvinceService } from '../_services/province.service';
import { HttpClient } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';
import { saveAs } from "file-saver";
import { NgxSpinnerService } from 'ngx-spinner';
import { DistrictService } from '../_services/district.service';
import { District } from '../_models/district';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  programs:Program[];
  provinces:Province[];
  districts:District[];
  reportmessage:any;
    constructor(
      private programService:ProgramService,
      private provinceService:ProvinceService,
      private http:HttpClient,
      private toastr:ToastrService,
      private spinner: NgxSpinnerService,
      private districtService:DistrictService
    ) { }
  
    ngOnInit() {
      this.LoadPrograms();
      this.LoadProvinces();
      this.LoadDistricts();
    }
    LoadPrograms(){
      this.programService.getAllPrograms().subscribe(data => {
        this.programs = data;
      });
    }
    LoadProvinces(){
      this.provinceService.getAllProvinces().subscribe(data => {
        this.provinces = data;
      });
    }
    LoadDistricts(){
      this.districtService.getAllDistricts().subscribe( data =>{
        this.districts = data;
      })
    }
    onDownload() {
      this.showSpinner();
      this.http
        .get(`http://localhost:8080/api/municipalities/municipalityreport`, {
          responseType: "blob",
          headers: { Accept: "application/pdf" }
        })
        .subscribe(blob => {
          saveAs(blob,"Report.pdf");
          this.toastr.show("Report Generated Successfully")
        });
    }
    showSpinner() {
      this.spinner.show();
      setTimeout(() => {
          /** spinner ends after 5 seconds */
          this.spinner.hide();
      }, 5000);
    }
}
