import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './_helpers/auth.guard';
import { InsertdataformComponent } from './insertdataform/insertdataform.component';
import { Forgotpassword1Component } from './forgotpassword1/forgotpassword1.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  {path: 'forgotpassword/email',component:Forgotpassword1Component},
  { path: 'insertdataform', component: InsertdataformComponent,canActivate:[AuthGuard] },
  { path: 'mainpage', loadChildren: () => import(`./mainpage/mainpage.module`).then(m => m.MainpageModule),canActivate:[AuthGuard]},
  { path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
