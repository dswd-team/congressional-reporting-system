
export interface Joindata {
    municipalityId: number;
    districtName: string;
    provinceName:string;
    municipalityName:string;
    municipalityServedBenes:number;
    municipalityAmountDisbursed:number;
    municipalityTargetBenes: number;
    municipalityAmountAllocated:number;
  }
  
