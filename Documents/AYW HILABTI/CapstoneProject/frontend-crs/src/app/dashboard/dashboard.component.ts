import { Component, OnInit } from '@angular/core';
import { Program } from '../_models/program';
import { Province } from '../_models/province';
import { ProgramService } from '../_services/program.service';
import { ProvinceService } from '../_services/province.service';
import { multi } from './data';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  programs:Program[];
  provinces:Province[];

  multi: any[];
  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  // gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  yAxisLabel: string = 'Programs';
  showYAxisLabel: boolean = true;
  xAxisLabel: string = 'Number of Beneficiaries and Amount Disbursed';
  legendTitle: string = 'Legend';

  colorScheme = {
    domain: ['#3498DB', '#334CFF', '#AAAAAA']
  };


  constructor(
    private programService:ProgramService,
    private provinceService:ProvinceService
  ) { 
    Object.assign(this, { multi })
  }

  ngOnInit() {
    this.LoadProvinces();
    this.LoadPrograms();
  }
  reloadPage() {
    window.location.reload();
  }
  LoadPrograms(){
    this.programService.getAllPrograms().subscribe(data => {
      this.programs = data;
    });
  }
  LoadProvinces(){
    this.provinceService.getAllProvinces().subscribe(data => {
      this.provinces = data;
    });
  }

  
 onSelect(data): void {
  console.log('Item clicked', JSON.parse(JSON.stringify(data)));
}

onActivate(data): void {
  console.log('Activate', JSON.parse(JSON.stringify(data)));
}

onDeactivate(data): void {
  console.log('Deactivate', JSON.parse(JSON.stringify(data)));
}
}
