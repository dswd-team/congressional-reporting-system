export var multi = [
    {
      "name": "Pantawid Pamilya",
      "series": [
        {
          "name": "Number of Beneficiaries",
          "value": 7300000
        },
        {
          "name": "Amount Disbursed",
          "value": 8940000
        }
      ]
    },
    
  
    {
      "name": "Sustainable Livelihood",
      "series": [
        {
          "name": "Number of Beneficiaries",
          "value": 7870000
        },
        {
          "name": "Amount Disbursed",
          "value": 8270000
        }
      ]
    },
  
    {
        "name": "Kalahi-CIDSS-NCDDP",
        "series": [
          {
            "name": "Number of Beneficiaries",
            "value": 7870000
          },
          {
            "name": "Amount Disbursed",
            "value": 8270000
          }
        ]
      },
      {
        "name": "Social Pension",
        "series": [
          {
            "name": "Number of Beneficiaries",
            "value": 7870000
          },
          {
            "name": "Amount Disbursed",
            "value": 8270000
          }
        ]
      },
      {
        "name": "Centenarian Law Implementation",
        "series": [
          {
            "name": "Number of Beneficiaries",
            "value": 7870000
          },
          {
            "name": "Amount Disbursed",
            "value": 7770000
          }
        ]
      },
      {
        "name": "Supplementary Feeding",
        "series": [
          {
            "name": "Number of Beneficiaries",
            "value": 7870000
          },
          {
            "name": "Amount Disbursed",
            "value": 8270000
          }
        ]
      },
  ];
  
  