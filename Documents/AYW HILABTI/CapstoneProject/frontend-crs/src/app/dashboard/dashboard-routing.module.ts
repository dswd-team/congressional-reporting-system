import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PieGridComponent } from './pie-grid/pie-grid.component';
import { HorizontalGraphComponent } from './horizontal-graph/horizontal-graph.component';


const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: 'pie-grid',component:PieGridComponent},
      { path: 'horizontal-graph',component:HorizontalGraphComponent},
      {
        path: '', redirectTo: 'horizontal-graph', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
