import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PieGridComponent } from './pie-grid/pie-grid.component';
import { HorizontalGraphComponent } from './horizontal-graph/horizontal-graph.component';

@NgModule({
  declarations: [DashboardComponent, PieGridComponent, HorizontalGraphComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxChartsModule,
  ]
})
export class DashboardModule { }
