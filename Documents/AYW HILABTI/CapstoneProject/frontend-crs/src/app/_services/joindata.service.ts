import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Joindata } from '../_models/joindata';

@Injectable({
  providedIn: 'root'
})
export class JoindataService {

  baseUrl: string = `${environment.baseUrl}/api/jointable`;

  constructor(private http: HttpClient) {}

    //Get all programs : /programs/all
    getLeftJoin(): Observable<Joindata[]> {
      return this.http.get<Joindata[]>(`${this.baseUrl}/left`);
    }

    getLeftJoinByProvinceId(id:number): Observable<Joindata[]> {
      return this.http.get<Joindata[]>(`${this.baseUrl}/byprovince/${id}`);
    }
    getLeftJoinByDistrictId(id:number): Observable<Joindata[]> {
      return this.http.get<Joindata[]>(`${this.baseUrl}/bydistrict/${id}`);
    }
}
