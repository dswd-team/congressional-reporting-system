import { TestBed } from '@angular/core/testing';

import { DbfileService } from './dbfile.service';

describe('DbfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DbfileService = TestBed.get(DbfileService);
    expect(service).toBeTruthy();
  });
});
