import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportstorageRoutingModule } from './reportstorage-routing.module';
import { ReportstorageComponent } from './reportstorage.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { NgbPaginationModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { UploadComponent } from './upload/upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { DeleteComponent } from './delete/delete.component';
import { DownloadComponent } from './download/download.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ReportstorageComponent, UploadComponent, DeleteComponent, DownloadComponent],
  imports: [
    CommonModule,
    ReportstorageRoutingModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    NgbModalModule,
    RouterModule,
    FileUploadModule,
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule ,
  ],
  entryComponents:[UploadComponent,DeleteComponent,DownloadComponent]
})
export class ReportstorageModule { }
