import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DbfileService } from 'src/app/_services/dbfile.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DBFile } from 'src/app/_models/dbfile';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  SERVER_URL = "http://localhost:8080/api/reports/uploadFile";
  uploadForm: FormGroup; 
  reports:DBFile[];
  constructor(
    private httpClient: HttpClient,
    private formBuilder: FormBuilder, 
    private toastr:ToastrService,
    private dbfileService:DbfileService,
    public activeModal: NgbActiveModal,
    private spinner: NgxSpinnerService
    ) {}

  ngOnInit(){
    this.uploadForm = this.formBuilder.group({
      reportfile: ['']
    });
    this.LoadReports();
  }
  LoadReports() {
    this.dbfileService.getAllReports().subscribe(data => {
      this.reports = data;
      console.log(this.reports);
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('reportfile').setValue(file);
    }
  }
  onSubmit() {
    this.showSpinner();
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('reportfile').value);

    this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
      (res) => {
        this.toastr.show("Upload Successful");
        this.LoadReports();
        this.activeModal.close();
        
      },() =>  this.toastr.error("Upload Failed"),
    );
    
  }
  showSpinner() {
    this.spinner.show();
    setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 1000);
  }

}
