import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportstorageComponent } from './reportstorage.component';

describe('ReportstorageComponent', () => {
  let component: ReportstorageComponent;
  let fixture: ComponentFixture<ReportstorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportstorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportstorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
