import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportstorageComponent } from './reportstorage.component';


const routes: Routes = [
  {
    path: "",
    component: ReportstorageComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportstorageRoutingModule { }
