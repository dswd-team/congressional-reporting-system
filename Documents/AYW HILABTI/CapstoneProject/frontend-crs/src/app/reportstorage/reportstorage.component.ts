import { Component, OnInit } from "@angular/core";
import { PdfReport } from "../_models/pdfreport";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PdfreportService } from "../_services/pdfreport.service";
import { UploadComponent } from "./upload/upload.component";
import { DomSanitizer } from "@angular/platform-browser";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { DBFile } from "../_models/dbfile";
import { DbfileService } from "../_services/dbfile.service";
import { DeleteComponent } from "./delete/delete.component";
import { saveAs } from "file-saver";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: "app-reportstorage",
  templateUrl: "./reportstorage.component.html",
  styleUrls: ["./reportstorage.component.css"]
})
export class ReportstorageComponent implements OnInit {
  pdfReports: any;
  fileUrl;
  selectedFile;
  reports: any;
  //initializing p to one
  p: number = 1;
  SERVER_URL = "http://localhost:8080/api/reports/uploadFile";
  uploadForm: FormGroup;
  constructor(
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private dbfileService: DbfileService,
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    // this.LoadPDFs();
    this.uploadForm = this.formBuilder.group({
      reportfile: [""]
    });
    this.getAllReports();
  }
  openUploadFrom() {
    this.modalService.open(UploadComponent, { size: "lg", centered: true });
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get("reportfile").setValue(file);
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append("file", this.uploadForm.get("reportfile").value);

    this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
      res => {
        this.getAllReports();
        this.toastr.show("Upload Successful");
      },
      () => this.toastr.error("Upload Failed")
    );
  }
  getAllReports() {
    this.dbfileService.getAllReports().subscribe(data => {
      this.showSpinner();
      this.reports = data;
      const blob = new Blob([this.reports.data], {
        type: this.reports.filetype
      });
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
        window.URL.createObjectURL(blob)
      );
      console.log(this.reports);
    });
  }

  onDelete(report: DBFile) {
    const modalRef = this.modalService.open(DeleteComponent);
    modalRef.componentInstance.report = report;
    modalRef.result.then(result => {
      if (!result) return null;
      this.dbfileService.deleteFile(result).subscribe(
        response => {
          if (response) {
            this.getAllReports();
            this.toastr.success("Delete Success");
          }
        },
        () => this.toastr.error("Delete Failed")
      );
    });
  }
  onDownload(id: String) {
    this.http
      .get(`http://localhost:8080/api/reports/downloadFile/${id}`, {
        responseType: "blob",
        headers: { Accept: "application/pdf" }
      })
      .subscribe(blob => {
        saveAs(blob, "download.pdf");
      });
  }
  showSpinner() {
    this.spinner.show();
    setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
    }, 1000);
  }
}
