import { Component, OnInit, Input } from '@angular/core';
import { DBFile } from 'src/app/_models/dbfile';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  @Input()
  report: DBFile;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}

  delete() {
    this.activeModal.close(this.report.id);
  }
}
