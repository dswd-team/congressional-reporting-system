import { Component, OnInit, Input } from '@angular/core';
import { DBFile } from 'src/app/_models/dbfile';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  @Input()
  report: DBFile;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}

  download() {
    this.activeModal.close(this.report.id);
  }

}
