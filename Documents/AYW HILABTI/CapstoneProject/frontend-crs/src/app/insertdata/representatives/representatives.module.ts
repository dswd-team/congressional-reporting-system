import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepresentativesRoutingModule } from './representatives-routing.module';
import { RepresentativesComponent } from './representatives.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [RepresentativesComponent],
  imports: [
    CommonModule,
    RepresentativesRoutingModule,
    FormsModule,
    RouterModule,
  ]
})
export class RepresentativesModule { }
