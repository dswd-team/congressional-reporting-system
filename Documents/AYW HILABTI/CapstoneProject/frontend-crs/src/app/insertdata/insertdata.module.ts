import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InsertdataRoutingModule } from './insertdata-routing.module';
import { InsertdataComponent } from './insertdata.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [InsertdataComponent],
  imports: [
    CommonModule,
    InsertdataRoutingModule,
    FormsModule,
    HttpClientModule,
  ]
})
export class InsertdataModule { }
