import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramsRoutingModule } from './programs-routing.module';
import { ProgramsComponent } from './programs.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ProgramformComponent } from './programform/programform.component';
import { DeleteprogramComponent } from './deleteprogram/deleteprogram.component';
import { NgbPaginationModule, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [ProgramsComponent, ProgramformComponent,DeleteprogramComponent],
  imports: [
    CommonModule,
    ProgramsRoutingModule,
    FormsModule,
    RouterModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbModule,
  ],
  entryComponents:[ProgramformComponent, DeleteprogramComponent]
})
export class ProgramsModule { }
